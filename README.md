# ProtonClient
[](!https://badger.edvinbasil.com/badge/gitlab/carnaged/protonclient/status.svg)

> Javascript client side for Proton backend server. 

## Build

```sh
npx webpack
```

## Run tests

```sh
npm run test
```
## Usage

### Auth
### Events
### Participants
### Colleges
