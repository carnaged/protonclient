const protonRequest = require('../helpers/protonRequest');

class Colleges {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
  }

  /**
   * [O] Gets a list of colleges.
   * @returns {Array} array of college objects
   */
  getAll() {
    const req = {
      ...this.baseOptions,
      url: '/colleges',
    };
    return protonRequest(req);
  }

  /**
   * [A] Add a college.
   * @param {Object} data an object containing college params.
   * @returns object with status message.
   */
  add(data) {
    const req = {
      ...this.baseOptions,
      method: 'POST',
      url: '/colleges',
      data,
    };
    return protonRequest(req);
  }
}

module.exports = Colleges;
