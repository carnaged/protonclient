const protonRequest = require('../helpers/protonRequest');

class Participant {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
  }

  /**
   * [O] Registers a participant
   * @param {Object} data object with auth data
   */
  register(data) {
    const req = {
      ...this.baseOptions,
      url: '/participant/register',
      method: 'POST',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [O] Get jwt token in response body
   * @param {Object} data object with auth data
   */
  getToken(data) {
    const req = {
      ...this.baseOptions,
      url: '/participant/get-token',
      method: 'POST',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [O] Sets jwt token as secure cookie
   * @param {Object} data object with auth data
   */
  setCookie(data) {
    const req = {
      ...this.baseOptions,
      url: '/participant/set-cookie',
      method: 'POST',
      data,
    };
    return protonRequest(req);
  }
}

// TODO: Maybe derive both admin and participant classes from a user class.

class Admin {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
  }

  /**
   * [Private] Registers an admin
   * @param {Object} data object with auth data
   */
  register(data) {
    const req = {
      ...this.baseOptions,
      url: '/admin/register',
      method: 'POST',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [O] Get jwt token in response body
   * @param {Object} data object with auth data
   */
  getToken(data) {
    const req = {
      ...this.baseOptions,
      url: '/admin/get-token',
      method: 'POST',
      data,
    };
    return protonRequest(req);
  }
}
class Auth {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
    this.participant = new Participant(baseOptions);
    this.admin = new Admin(baseOptions);
  }
}
module.exports = Auth;
