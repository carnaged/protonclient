const protonRequest = require('../helpers/protonRequest');

class Events {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
  }

  /**
   * [APO] Gets a list of events.
   * @returns {Array} array of event objects
   */
  getAll() {
    // const opt = options.events[this.getAll.name];
    const req = {
      ...this.baseOptions,
      url: '/events',
    };
    return protonRequest(req);
  }

  /**
   * [APO] Gets data on a single event.
   * @param {String} id Id of the event to fetch
   * @returns {object} event object
   */
  get(id) {
    // const opt = options.events[this.get.name]
    const req = {
      ...this.baseOptions,
      url: `/events/${id}`,
    };
    return protonRequest(req);
  }

  /**
   * [A] Add an event.
   * @param {Object} data an object containing the event params
   * @returns object with message and event object if successful
   */
  add(data) {
    const req = {
      ...this.baseOptions,
      method: 'POST',
      url: '/events',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [A] Edit an event
   * @param {String} id Id of the event to edit
   * @param {Object} data object containing params to edit
   * @returns message and edited event object
   */
  edit(id, data) {
    const req = {
      ...this.baseOptions,
      method: 'PUT',
      url: `/events/${id}`,
      data,
    };
    return protonRequest(req);
  }

  /**
   * [A] Publish and unpublish events
   * @param {Object} data publish object
   * @returns message showing published status
   */
  publsh(data) {
    const req = {
      ...this.baseOptions,
      method: 'POST',
      url: '/events/publish',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [O] List participants of a particular event
   * @param {String} id id of the event
   * @returns event_id, participant count and array of participants
   */
  getParticipants(id) {
    const req = {
      ...this.baseOptions,
      url: `/events/${id}/participants`,
    };
    return protonRequest(req);
  }
}

module.exports = Events;
