const protonRequest = require('../helpers/protonRequest');

class Colleges {
  constructor(baseOptions) {
    this.baseOptions = baseOptions;
  }

  /**
   * [P] Get participant object of logged in user
   */
  getSelf() {
    const req = {
      ...this.baseOptions,
      url: '/participants/self',
    };
    return protonRequest(req);
  }

  /**
   * [P] Edit participant object of logged in user
   */
  putSelf(data) {
    const req = {
      ...this.baseOptions,
      method: 'PUT',
      url: '/participants/self',
      data,
    };
    return protonRequest(req);
  }

  /**
   * [P] Delete the logged in user
   */
  deleteSelf() {
    const req = {
      ...this.baseOptions,
      method: 'DELETE',
      url: '/participants/self',
    };
    return protonRequest(req);
  }

  /**
   * [A] Get list of participants
   */
  getAll() {
    const req = {
      ...this.baseOptions,
      url: '/participants',
    };
    return protonRequest(req);
  }

  /**
   * [A] Get participant by id
   * @param {String} id id or short_id of participant
   */
  getOne(id) {
    const req = {
      ...this.baseOptions,
      url: `/participants/${id}`,
    };
    return protonRequest(req);
  }

  /**
   * [A] Edit participant object by id
   * @param {String} id id of participant to edit
   * @param {Object} data modified participant object
   */
  editOne(id, data) {
    const req = {
      ...this.baseOptions,
      method: 'PUT',
      url: `/participants/${id}`,
      data,
    };
    return protonRequest(req);
  }

  /**
   * [A] Delete participant by id
   * @param {String} id id of participant to delete
   */
  deleteOne(id) {
    const req = {
      ...this.baseOptions,
      method: 'DELETE',
      url: `/participants/${id}`,
    };
    return protonRequest(req);
  }
}

module.exports = Colleges;
