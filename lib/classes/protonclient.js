const Events = require('./events');
const Colleges = require('./colleges');
const Auth = require('./auth');
/**
 * Client for proton
 *
 */
class ProtonClient {
  /**
   * Client for proton
   * @param {String} url proton backend url
   */
  constructor(url) {
    this.BASEURL = url;
    this.baseOptions = {
      method: 'get',
      baseURL: this.BASEURL,
    };
    // this.setEndpoints();
    // this.requests = {};
    this.events = new Events(this.baseOptions);
    this.colleges = new Colleges(this.baseOptions);
    this.auth = new Auth(this.baseOptions);
  }
}

module.exports = ProtonClient;
