const Axios = require('axios');

function protonRequest(opts) {
  return Axios(opts)
    .then(res => res.data)
    .catch(err => err.message);
}

module.exports = protonRequest;
